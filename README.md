# Hugo Build Env

[![pipeline status](https://gitlab.com/kicad/kicad-ci/hugo-build-env/badges/main/pipeline.svg)](https://gitlab.com/kicad/kicad-ci/hugo-build-env/-/commits/main)

[Hugo](https://github.com/gohugoio/hugo) docker image, mainly for site deployment.


## Versioning

This repo opts to use semantic versioning for internal use of hugo and asciidoctor.

## Building

The dockerfile takes hugo and asciidoctor versions as a paramter and that should be all:

```
docker build . --build-arg HUGO_VERSION=0.77.0 --build-arg ASCIIDOCTOR=2.0.17
```

## Uploading containers to registry

Remeber that you need to be authenticated against gitlab registry. If not, you can do so by executing the following command:
```
docker login registry.gitlab.com -u <username> -p <token>
```

When building the image, aproprietly tag it:
```
docker build . --build-arg HUGO_VERSION=0.77.0 --build-arg ASCIIDOCTOR=2.0.17 -t registry.gitlab.com/kicad/kicad-ci/hugo-build-env:1.0.0
```

Finally, push the image:
```
docker push registry.gitlab.com/kicad/kicad-ci/hugo-build-env:1.0.0
```

## CI

This repo is able to automatically rebuild the latest version of the tools on a push to the `main` branch. If a tag exists with a version number, that version will be built using the code in that particular tag.
