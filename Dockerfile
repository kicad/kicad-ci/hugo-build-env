# Alpine sounds like a good starting point
FROM alpine

ARG HUGO_VERSION
ARG ASCIIDOCTOR_VERSION

# Add asciidoctor on top and clean afterwards
RUN test -n "${ASCIIDOCTOR_VERSION}" || { echo "Please provide an asciidoctor version!"; exit 1; } && \
 apk --no-cache add ruby && \
 gem install coderay asciidoctor:${ASCIIDOCTOR_VERSION} --no-document && \
 find /tmp -mindepth 1 -maxdepth 1 | xargs rm -rf

# Last but not least, hugo
ENV HUGO_ENV=production
RUN test -n "${HUGO_VERSION}" || { echo "Please provide a hugo version!"; exit 1; } && \
 apk --no-cache add --virtual build-dependencies curl && \
 curl -o /tmp/hugo_${HUGO_VERSION}_Linux-64bit.tar.gz -sSL https://github.com/gohugoio/hugo/releases/download/v${HUGO_VERSION}/hugo_${HUGO_VERSION}_Linux-64bit.tar.gz && \
 tar xz -C /tmp -f /tmp/hugo_${HUGO_VERSION}_Linux-64bit.tar.gz && \
 cp /tmp/hugo /usr/local/bin/ && \
 apk --no-cache del build-dependencies && \
 find /tmp -mindepth 1 -maxdepth 1 | xargs rm -rf
